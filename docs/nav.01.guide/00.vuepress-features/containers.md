# Containers

Vuepress offers a neat syntax for highlighting important content.

::: tip A remark
With some content.
:::

::: warning A kind warning
With some content.
:::

::: details A long story, click to open
With some longer content.
:::
