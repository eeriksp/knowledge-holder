# Quotations

The `<Quote>` component will help you to highlight important words:

```
<Quote author="Śāntideva">
All other virtues are like plantain trees;
After coming to fruition they simply cease to be.
But the tree of bodhicitta constantly
Gives fruit and increases unceasingly.
</Quote>
```

<Quote author="Śāntideva">All other virtues are like plantain trees;<br>After coming to fruition they simply cease to be.<br>But the tree of bodhicitta constantly<br>Gives fruit and increases unceasingly.</Quote>


The `author` property is optional since the author might not always be known.

:::warning Limitation
Currently there are not newlines allowed in the quote. Use `<br>` instead. Hopefully this is fixed in the future.

:::
