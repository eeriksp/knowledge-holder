# Phrase translations

It often happens that you want to include a phrase in many different languages. For instance, you have your notes in English, but need to include philosophical terms in Sanskrit or Latin as well.

 You can use `<OtherLanguage>` component for that:
 
 ```
The Four Nobel Truths
<OtherLanguage lang="skr">catvāri āryasatyāni</OtherLanguage>
```
will display the Sanskrit word as a tooltip (hover over to see):

The Four Nobel Truths <OtherLanguage lang="skr">catvāri āryasatyāni</OtherLanguage>

## English

For English language the shortcut `<Eng>` component can be used:

```
*sarva samskara anitya* 
<Eng>All which is compounded is impermanent</Eng>
```

renders as:

*sarva samskara anitya* <Eng>All which is compounded is impermanent</Eng>

To simplify further, the `#eng{phrase}` notation is made available:

```
*sarva samskara anitya* #eng{All which is compounded is impermanent}
```

renders as:

*sarva samskara anitya* #eng{All which is compounded is impermanent}

## Tibetan

A similar shorthand notation exists for Tibetan:

```
Buddha #tib{Sangyé} is Awakened Omniscience
```

renders as:

Buddha #tib{Sangyé} is Awakened Omniscience
