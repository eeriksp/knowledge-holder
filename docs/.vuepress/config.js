const getConfig = require('vuepress-bar')

module.exports = {
  title: 'Knowledge holder',
  base: '',
  dest: 'public',
  markdown: {
    extendMarkdown: md => {
      md.use(require('markdown-it-sup'))
      const Plugin = require('markdown-it-regexp')

      // English translation
      md.use(Plugin(/#eng{(.+?)}/, function (match, utils) {
        return `<Eng>${match[1]}</Eng>`
      }))

      // Tibetan translation
      md.use(Plugin(/#tib{(.+?)}/, function (match, utils) {
        return `<OtherLanguage lang="tib">${match[1]}</OtherLanguage>`
      }))

      // Note
      md.use(Plugin(/#note{(.+?)}/, function (match, utils) {
        return `<Note>${match[1]}</Note>`
      }))

    }
  },
  themeConfig: {
    logo: '/img/orange-dharmawheel.svg',
    // create navbar and sidebar automatically from directory tree
    ...getConfig(`${__dirname}/..`, { addReadMeToFirstGroup: false }),
    nextLinks: false,
    prevLinks: false
  },
  plugins: [
    '@markspec/vuepress-plugin-footnote',
  ],
  head: [
    ['link', { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Noto+Serif&display=swap' }],
  ],
  evergreen: true
}
