---
home: true
heroImage: /img/white-dharmawheel.svg
heroText: Knowledge Holder
tagline: A Vuepress-powered application for organizing your notes.
actionText: Get Started →
actionLink: /nav.00.quickstart/
features:
- title: Preconfigured
  details: This is a Vuepress project template preconfigured for a note taking application. 
- title: Vue(press)-Powered
  details: Enjoy the beautiful Vuepress layout with possibility to develop addons with Vue.
- title: Open-source
  details: Licensed under MIT licence.
footer: May all beings have happiness and the causes of happines.
---
