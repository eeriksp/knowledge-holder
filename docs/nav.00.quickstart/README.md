---
sidebar: false
---

# Getting started

## Setup

1. Clone the repository
1. cd
1. download dependencies `npm i`
1. start dev server `vuepress dev`

## Technologies used

* [Vuepress](https://vuepress.vuejs.org/)
* [Vuetify](https://vuetifyjs.com/en/)

Since this project is just a thin wrapper around Vuepress, refer to its documentation for a detailed config reference.

Features specific to this project are documented [here](/nav.00.quickstart/features/)

To see the full power of this application, have a look at the [sample page](/nav.01.sample/).
